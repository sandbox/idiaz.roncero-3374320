<?php

namespace Drupal\sdc_story_generator\Generators;

use Drupal\sdc\ComponentPluginManager;
use Drupal\sdc\Plugin\Component;
use DrupalCodeGenerator\Command\DrupalGenerator;
use Symfony\Component\Console\Question\Question;

/**
 * A Drush commandfile.
 */
final class StoryGenerator extends DrupalGenerator {

  /**
   * {@inheritdoc}
   */
  protected ?int $extensionType = self::EXTENSION_TYPE_THEME;

  /**
   * {@inheritdoc}
   */
  protected bool $isNewExtension = FALSE;

  /**
   * {@inheritdoc}
   */
  protected string $name = 'theme:sdc:story';

  /**
   * {@inheritdoc}
   */
  protected string $description = 'Generates a Storybook story from a SDC component.';

  /**
   * {@inheritdoc}
   */
  protected string $alias = 'story';

  /**
   * {@inheritdoc}
   */
  protected string $templatePath = __DIR__;

  /**
   * The SDC Component plugin manager.
   *
   * @var \Drupal\sdc\ComponentPluginManager
   */
  protected $componentPluginManager;

  /**
   * Constructs a StoryGenerator object.
   */
  public function __construct(ComponentPluginManager $component_plugin_manager) {
    parent::__construct($this->name);
    $this->componentPluginManager = $component_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function generate(&$vars): void {
    $vars = $this->askQuestions($vars);
    $this->generateAssets($vars);
  }

  /**
   * Asks the questions to the user.
   *
   * @param array $vars
   *   The answers array passed by reference.
   *
   * @return array
   *   The answers to the questions.
   */
  private function askQuestions(array &$vars): array {
    $component_id = $this->askComponentName($vars);

    // We don't perform validation on askComponentName because we rely on
    // componentPluginManager->find() to throw an error if component is
    // nonexistent.
    $component = $this->componentPluginManager->find($component_id);
    $component_data = $component->metadata;

    // Path and machineName will be used on generateAssets
    // and need to be stored into $vars.
    $vars['component_path'] = $component_data->path;
    $vars['component_machine_name'] = $component_data->machineName;

    // Get the component provider (module or theme)
    $provider = $component->getPluginDefinition()['provider'];
    $extensions = [
      ...$this->drupalContext->getModules(),
      ...$this->drupalContext->getThemes(),
    ];
    $extension_name = $extensions[$provider];

    // Opinionated: the story name takes the form Extension/ComponentName.
    $vars['story_name'] = "$extension_name/$component_data->name";

    // Merge props and slots as this is a SDC concept: everything is an argType
    // for Storybook. Identify slots for later use.
    $props = $component_data->schema['properties'];
    $slots = array_map(function (array $slot) {
      return array_merge($slot, ['slot' => TRUE]);
    }, $component_data->slots);
    $args = array_merge($props, $slots);

    if ($args) {
      $required = $component_data->schema['required'];
      $vars['story_props'] = [];

      foreach ($args as $arg_name => $arg_data) {
        $is_slot = !empty($arg_data['slot']) && $arg_data['slot'];
        $arg_definition = [
          'name' => $arg_name,
        ];

        if (!empty($arg_data['title'])) {
          $arg_definition['title'] = $arg_data['title'];
        }

        $type = is_array($arg_data['type']) ? $arg_data['type'][0] : $arg_data['type'];
        $arg_definition['type'] = $type;

        if ($required && in_array($arg_name, $required)) {
          $arg_definition['required'] = $required;
        };

        if (!empty($arg_data['description'])) {
          $arg_definition['description'] = $arg_data['description'];
        };

        $arg_definition['category'] = $is_slot ? 'Slots' : 'Props';

        if (!empty($arg_data['enum'])) {
          $arg_definition['options'] = $arg_data['enum'];
        };

        if (!empty($arg_data['default'])) {
          $arg_definition['default'] = $arg_data['default'];
        };

        $vars['story_args'][$arg_name] = $arg_definition;
      }
    }

    return $vars;
  }

  /**
   * Create the assets that the framework will write to disk later on.
   *
   * @param array $vars
   *   The answers to the CLI questions.
   */
  private function generateAssets(array $vars): void {
    $component_path_token = '{component_path}' . DIRECTORY_SEPARATOR;
    $this->addFile($component_path_token . "{component_machine_name}.stories.yml", 'stories-yml--template.twig');
  }

  /**
   * Asks the component name, with autocomplete helper.
   *
   * @param array $vars
   *   The generator variables.
   *
   * @return string
   *   The component ID.
   */
  protected function askComponentName(array $vars): string {
    $component_question = new Question('What is the component ID? (i.e: mytheme:mycomponent)', '');
    $component_ids = array_map(function (Component $component) {
      return $component->getPluginId();
    }, $this->componentPluginManager->getAllComponents());
    $component_question->setAutocompleterValues(\array_values($component_ids));
    return $this->io->askQuestion($component_question);
  }

}
