# SDC Story generator

## TL;DR

Use `drush generate theme:sdc:story` or `drush generate story` to convert a Drupal
Single Directory Component `*.yml` definition to a Storybook Component Story
Format `*.yml` file.

## Which version to use:

Due to breaking changes on Drush generators between 11.x and 12.x, this
module has two different versions:

- `1.0.x` supports Drush^11.x
- `2.0.x` supports Drush^12.x

## Why

Drupal's Single Directory Components (SDC) requires components to be defined via
a `mycomponent.component.yml` file. See [here](https://www.drupal.org/docs/develop/theming-drupal/using-single-directory-components/annotated-example-componentyml) for an
annotated example and schema details.

Storybook follows the open standard Component Story Format (CSF) ([docs](https://storybook.js.org/docs/react/api/csf))
and can be integrated into Drupal using modules such as [CL Server](https://www.drupal.org/project/cl_server).

You can find a reference on how to write a `mycomponent.stories.yml` [here](https://gitlab.com/-/snippets/2556203).

### The problem

Storybook can automatically infer most of the CSF data from libraries such
as React or Vue, but for Drupal we need to manually pass the information using
`JSON` or `YML` formats.

Since both formats (SDC and CSF) are very similar but incompatible, the process
of manually converting from one to the other can easily become cumbersome
and annoying.

For example, compare these files that define the same props for both Drupal
and Storybook:

`my-component.component.yml`

```yml
name: My Component
status: experimental
props:
  type: object
  required:
    - type
  properties:
    type:
      type: string
      title: Type
      description: Component type
      default: 'primary'
      enum:
        - primary
    rounded:
      type: boolean
      title: Rounded corners
    attributes:
      type: Drupal\Core\Template\Attribute
```

`my-component.stories.yml`

```yml
name: 'My Theme/My Component'
argTypes:
  type:
    name: type
    type:
      name: string
      required: true
    description: 'Component type'
    table:
      type:
        summary: 'primary | secondary'
      defaultValue:
        summary: primary
    options:
      - primary
      - secondary
  rounded:
    name: rounded
    type: boolean
    table:
      type:
        summary: boolean
  attributes:
    name: attributes
    type: Drupal\Core\Template\Attribute
    table:
      type:
        summary: Drupal\Core\Template\Attribute
```

## How to use

Install the module and run `drush generate theme:sdc:story` or `drush story`.

Identify your component by using its full ID, this is, `mytheme:mycomponent` or
`mymodule:mycomponent`. Autocompletion is enabled.

With this information, the drush command will locate your component
and produce the resultant `*.stories.yml` file on the component directory.
